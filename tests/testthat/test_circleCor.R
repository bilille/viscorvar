test_that("test of the circleCor function",{
  
  load(system.file("extdata", "res_block_splsda.rda", package="visCorVar"))
  load(system.file("extdata", "block_Y.rda", package="visCorVar"))
  
  mrna = res_block_splsda$X$mrna
  mirna = res_block_splsda$X$mirna
  protein = res_block_splsda$X$protein
  
  feature_mrna = colnames(mrna)[1:3]
  feature_mirna = colnames(mirna)[1:3]
  feature_protein = colnames(protein)[1:3]
  response_variables = colnames(block_Y)
  
  list_dataframe_cor_comp_var_global = list()
  
  x_mrna = c(0.2, 0.3, 0.9)
  y_mrna = c(0.2, 0.3, 0.4)
  
  data_mrna = data.frame(variable = feature_mrna,
                         block = rep("mrna", length(feature_mrna)),
                         cor_var_comp1 = x_mrna,
                         cor_var_comp2 = y_mrna)
  
  x_mirna = rep(0.2, 3)
  y_mirna = c(0.2, 0.75, 0.9)
  
  data_mirna = data.frame(variable = feature_mirna,
                          block = rep("mirna", length(feature_mirna)),
                          cor_var_comp1 = x_mirna,
                          cor_var_comp2 = y_mirna)
  
  x_protein = c(0.2, 0.75, 0.9)
  y_protein = rep(0.2, 3)
  
  data_protein = data.frame(variable = feature_protein,
                            block = rep("protein", length(feature_protein)),
                            cor_var_comp1 = x_protein,
                            cor_var_comp2 = y_protein)
  
  x_response_variables = c(0.7, 0.1, 0.9)
  y_response_variables = c(0.1, 0.7, 0.3)
  
  data_response_variables = data.frame(variable = response_variables,
                                       block = rep("Y", length(response_variables)),
                                       cor_var_comp1 = x_response_variables,
                                       cor_var_comp2 = y_response_variables)
  
  
  list_dataframe_cor_comp_var_global[[1]] = rbind(data_mrna,
                                                  data_mirna,
                                                  data_protein,
                                                  data_response_variables)
  
  names(list_dataframe_cor_comp_var_global) = "mrna-mirna-protein"
  
  
  names_blocks = c("mrna",
                   "mirna",
                   "protein")
  
  
  list_vec_index_block_select = list()
  list_vec_index_block_select[[1]] = c(1:3)
  mat_cor_comp1 = mat_cor_comp2 = matrix(0.85,
                                         nrow = 3,
                                         ncol = 3)
  colnames(mat_cor_comp1) = colnames(mat_cor_comp2) = names_blocks
  diag(mat_cor_comp1) = diag(mat_cor_comp2) = 1
  
  pdf(NULL)
  grDevices::dev.control(displaylist = "enable")
  
  selected_variables = circleCor(list_dataframe_cor_comp_var_global = list_dataframe_cor_comp_var_global,
                                 list_vec_index_block_select = list_vec_index_block_select,
                                 mat_cor_comp1 = mat_cor_comp1,
                                 mat_cor_comp2 = mat_cor_comp2,
                                 names_blocks = names_blocks,
                                 names_response_variables = response_variables,
                                 comp = 1:2,
                                 cutoff = 0.85,
                                 min.X = 0.5,
                                 max.X = 1,
                                 min.Y = -1,
                                 max.Y = 1,
                                 vec_col = colorRampPalette(brewer.pal(9, "Spectral"))(dim(mat_cor_comp1)[1] + 1),
                                 rad.in = 0.5,
                                 cex = 0.7,
                                 cex_legend = 0.8,
                                 pos = c(1.2, 0),
                                 pch = 20)
  
  recordplot_circleCor = grDevices::recordPlot()
  invisible(dev.off())
  
  # mrna
  x_mrna_test = recordplot_circleCor[[1]][[11]][[2]][[2]]$x[1]
  y_mrna_test = recordplot_circleCor[[1]][[11]][[2]][[2]]$y[1]
  
  cond_mrna = (x_mrna[3] == x_mrna_test & y_mrna[3] == y_mrna_test)

  # protein
  x_protein_test = recordplot_circleCor[[1]][[13]][[2]][[2]]$x[1]
  y_protein_test = recordplot_circleCor[[1]][[13]][[2]][[2]]$y[1]
  
  cond_protein = (x_protein[3] == x_protein_test & y_protein[3] == y_protein_test)
  
  # response variables
  x_response_variables_test = recordplot_circleCor[[1]][[15]][[2]][[2]]$x
  y_response_variables_test = recordplot_circleCor[[1]][[15]][[2]][[2]]$y
  
  cond_response_variables = (all(x_response_variables_test == x_response_variables) & all(y_response_variables == y_response_variables_test))
  
  cond = cond_mrna  & cond_protein & cond_response_variables
  
  expect_true(cond)
  
})