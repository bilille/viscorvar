visCorVar
========

The R package visCorVar allows visualizing results from a data integration with block.splsda or block.spls function (the circos plot cannot be plotted with the output of the block.spls function) from the mixOmics package. The data integration is performed for different types of \`omics datasets (transcriptomics, metabolomics, metagenomics) in order to select features of a \`omics dataset which are correlated with the features of the other \`omics datasets and the response variables and to predict the class membership of a new sample. These correlated variables can be visualized with a correlation circle, a network and a circos plot.

How to install visCorVar?
------------------------

### Ubuntu

For Ubuntu distribution, the package libgl1-mesa-glx has to be installed :
`sudo apt-get install libgl1-mesa-glx`.

### Windows

Rtools (https://cran.r-project.org/bin/windows/Rtools/) has to be installed before installing the package visCorVar.

### Within R

To install the visCorVar package from gitlab, open a R session and :
- Install the package mixOmics (http://mixomics.org/install/)
```r
if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")

BiocManager::install("mixOmics")
```
- Install the package igraph : `install.packages("igraph")`.
- Install the package remotes : `install.packages("remotes")`
- Install the package visCorVar : 
```r
library(remotes)
remotes::install_gitlab("bilille/viscorvar", build_vignettes = TRUE)
```

### Using conda


How to use visCorVar?
--------------------

Complete description and examples can be found in the R package vignette.
```r
RShowDoc("visCorVar_html", type = "html", package = "visCorVar")
RShowDoc("visCorVar", type = "pdf", package = "visCorVar")
```

### Data integration

Before using the visCorVar package, a data integration has to be performed with the block.splsda function or the block.spls function (mixOmics package). The data integration enables us to retrieve relevant features of a block which are highly correlated with the features of other blocks and the response variables.

```r 
res_data_integration = block.splsda(X = list_X,
                                    Y = factor_Y,
                                    ncomp = ncomp,
                                    keepX = keepX,
                                    design = design,
                                    scheme = scheme,
                                    max.iter = max_iter)
```

### Preprocessing

The preprocessing allows determining the blocks whose correlation circles can be overlaid and to compute the correlations between the components and the selected features, the correlations between the components and the response variables and optionnally the correlations between the components and the features of interest. This preprocessing is done by the matCorAddVar function. This preprocessing is required to plot the correlation circle, to create a network and to plot the circos plot. In this section, some parameters are detailed and an example of a call of the function matCorAddVar is given.

Each column of the block_Y matrix is associated with a response variable. In this example, the response has two categories (A and B). For the block.splsda function, the response variable is equal to
1 if the sample is in this category and is equal to 0 otherwise. The table below shows an example of block_Y :


The features of interest are features that the user want to have in the network and in the circos plot. The
features of interest have to be block variables. The table below shows an example of
features of interest.

```r
comp = 1:2
cutoff_comp = 0.8
res_matCorAddVar = matCorAddVar(res_block_splsda = res_data_integration,
                                block_Y = block_Y,
                                cutoff_comp = cutoff_comp,
                                var_interest = var_interest,
                                comp = comp)
```

The description of the input parameters is provided in the help of the function matCorAddVar.

### Correlation circle 

If it is possible to overlay the correlation circles, the function circleCor performs the overlaying of blocks and a zoom
in a rectangle of correlation circle. Only the selected features whose correlation with comp[1] component or comp[2]
component is greater than cutoff in absolute value are plotted in the correlation circle. The function circleCor returns a subset of relevant correlated features contained in the rectangle. The intra-blocks correlations and inter-blocks correlations between these features can be studied thanks to the network and the circos plot.


```r
library(RColorBrewer)
list_cor_comp_selected_var_resp_var = res_matCorAddVar$list_cor_comp_selected_var_resp_var
list_vec_index_block_select = res_matCorAddVar$list_vec_index_block_select
mat_cor_comp1 = res_matCorAddVar$mat_cor_comp1
mat_cor_comp2 = res_matCorAddVar$mat_cor_comp2
names_blocks = c("X1", "X3")
names_response_variables = c("A", "B")
comp = 1:2
vec_col = colorRampPalette(brewer.pal(9, "Spectral"))(dim(mat_cor_comp1)[1] + 1)
names_block_variables=circleCor(list_dataframe_cor_comp_var_global=list_cor_comp_selected_var_resp_var,    
                                list_vec_index_block_select = list_vec_index_block_select,
                                mat_cor_comp1 = mat_cor_comp1,
                                mat_cor_comp2 = mat_cor_comp2,
                                names_blocks = names_blocks,
                                names_response_variables = names_response_variables,
                                comp = comp,
                                cutoff = 0.85,
                                min.X = -1,
                                max.X = 1,
                                min.Y = -1,
                                max.Y = 1,
                                vec_col = vec_col,
                                rad.in = 0.5, 
                                cex = 0.7,
                                cex_legend = 0.8,
                                pos = c(1.2, 0),
                                pch = 20)
```

The description of the input parameters is provided in the help of the function circleCor.


### Network

A network can be created with the function networkVar and can be exported in the graphml format for visualization with
Cytoscape. This network higlights the inter-blocks correlations. An approximation of the correlation between two features (called similarity between two features) or the correlation can be associated with the link between two features. Before creating the network with the function networkVar, similarity 
matrices (or correlation matrices) have to be computed with the function computeMatSimilarity (or the function computeMatCor).

```r
res_compute_mat_similarity = computeMatSimilarity(res_matCorAddVar = res_matCorAddVar)
```

The description of the input parameters is provided in the help of the function computeMatSimilarity (or the function compuetMatCor).

The nodes of this network are the selected features, the response variables and optionnally the features of interest. Only the selected features whose similarity (or the correlation) with another feature is greater than a threshold
in absolute value are plotted. This network gives a insight of correlated variables.

```r
names_blocks = c("X1", "X3")
names_response_variables = c("A", "B")
comp = 1:2
names_resp_var2 = c("A")
res_networkVar = networkVar(res_compute_mat = res_compute_mat_similarity,
                            type_cor = "similarity",
                            names_selected_variables = names_block_variables,
                            names_response_variables = names_resp_var2,
                            cutoff = 0)
```

The description of the input parameters is provided in the help of the function networkVar.

### Circos plot

The circosPlotVar function enables us to visualize intra-blocks and inter-blocks correlations thanks to a circos plot. A line around the circos plot shows the mean of a feature for the samples associated with a category. An approximation of the correlation between two features (called similarity between two features) or the correlation can be associated with the link between two features. Before creating the circos plot with the function circosPlotVar, similarity 
matrices (or correlation matrices) have to be computed with the computeMatSimilarity function (or the computeMatCor function).

```r
res_circosPlotVar = circosPlotVar(res_compute_mat = res_compute_mat_similarity,
                                  vec_variables = names_block_variables,
                                  type_cor = "similarity",
                                  var.names = NULL,
                                  size_variables = 0.25,
                                  cutoff = 0.7,
                                  color_Y = c("green", "blue", "red"),
                                  color_blocks = c("brown1", "brown3",
                                                   "darkgoldenrod", "darkgoldenrod1",
                                                   "antiquewhite1", "antiquewhite3"),
                                  color_cor = c("red", "blue"),
                                  show_intra_links = FALSE,
                                  line = TRUE,
                                  fig_size = 800,
                                  segment_width = 25,
                                  line_plot_width = 90,
                                  size_legend = 0.8,
                                  ncol_legend = 1,
                                  size_labels = 1,
                                  legend = TRUE)
```
