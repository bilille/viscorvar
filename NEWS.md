## version 0.10

### new features / enhancements / changes
* test of the circleCor function
* add importDIABLO, importRGCCA and importSGCCA functions
* documentation of importDIABLO, importRGCCA, importSGCCA, circleCor, circosPlotVar and
matrixCorComp functions
* choice between similarity and correlation for the network and the circos plot
* add input parameters to circleCor function

### minor improvements
* update of the vignette in HTML and PDF format
* update DESCRIPTION file and NAMESPACE file
* corrections : corVarBlockOverlay.R, circleCor.R and network.R
* update example data

## version 0.9

### new features / enhancements / changes
* updated HTML and PDF vignettes
* updated documentation of matCorAddVar, circleCor, computeMatSimilarity and networkVar functions
* updated inst/extdata data
* updated examples

### minor improvements
* cleaned duplicated code


## version 0.8

### new features / enhancements / changes
* add dplyr and reshape2 dependencies in Imports field. These dependencies are required for the circosPlotVarSim and circosPlotVarCor functions
* change the name of the networkVar function to networkVarSim
* add computeMatCor, networkVarCor, networkVar, circosPlotVarCor, circosPlotVarSim, circosPlotVar functions
* documentations of the computeMatCor, networkVarSim, networkVarCor, networkVar, circosPlotVarCor, circosPlotVarSim, circosPlotVar functions
* add to the vignette a section describing the circosPlotVar function
* add to the README.md file a section describing the circosPlotVar function

### bug fixes

* computeMatSimilarity function : features of interest can belong blocks of some group of blocks AND NO, for each group of block, there is at least one feature of interest belonging to a block of this group of block


## version 0.7

### new features / enhancements / changes

* add vignettes in the HTML and PDF format
* add in README.md that, for Windows, Rtools has to be installed before installing the package visCorVar
* test that we can visualize the vignette in the PDF format

### bug fixes

* function computeMatSimilarity : indj2 is a vector of booleans indicating the variables of
interest which are selected block variables AND NOT the index indicating the variables of
interest which are selected block variables


## version 0.6

### new features / enhancements / changes

* added vignette pdf
* improved README
* improved DESCRIPTION
* improved documentation generation (roxygen)
* added NEWS.md
* added LICENCE
* moved example data to inst/extdata
* `circleCor`: take into account the correlation between the components of the first block of the group of blocks and other components
* added startup message

### bug fixes

### minor improvements


## version 0.5

### new features / enhancements / changes

* add the functions matCorAddVar, circleCor, computeMatSimilarity and networkVar
* add documentation for the functions matCorAddVar, circleCor, computeMatSimilarity and networkVar and for the example data
* add R Markdown for the vignette
* add DESCRIPTION, NAMESPACE, tests
* add instructions to install the dependencies of the package visCorVar in README.md
* add example data  

### bug fixes

* function circleCor : as we compute the correlation between the response variables and the components of the first block of the group of blocks, we have to take into account the sign of the correlation between the components of the first block of the group of block and the components of the block of a sub
group of blocks AND NOT the correlation between the components of the first block of the sub group of blocks and the components of the block of a sub group of blocks
* function networkVar : we check that the variables of interest are block variables AND NOT that the variables of interests are selected block variables
* function computeMatSimilarity : we have to check that for a block the length of the variables of interest is different from zero.
  
### minor improvements
